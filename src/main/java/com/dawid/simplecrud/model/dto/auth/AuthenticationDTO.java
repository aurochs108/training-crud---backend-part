package com.dawid.simplecrud.model.dto.auth;

public class AuthenticationDTO {
    private String message;

    public AuthenticationDTO(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
