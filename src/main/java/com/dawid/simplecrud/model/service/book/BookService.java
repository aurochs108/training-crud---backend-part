package com.dawid.simplecrud.model.service.book;

import com.dawid.simplecrud.model.entity.book.Book;
import com.dawid.simplecrud.model.repository.BookRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BookService {

    private BookRepository bookRepository;

    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public Iterable<Book> getAllBooks() {
        return bookRepository.findAllByOrderById();
    }

    public String saveBook(Book book) {
        try {
            bookRepository.save(book);
        } catch (Exception e) {
            return "error! book not added to DB";
        }
        return "book added";
    }

    public Optional<Book> findBookById(int id) {
        Optional<Book> optionalBookById = bookRepository.findById(id);
        if (optionalBookById.isPresent()) {
            return optionalBookById;
        }
        return Optional.empty();
    }

    public String deleteBookById(int id) {
        Optional<Book> optionalBookById = findBookById(id);
        if (optionalBookById.isPresent()) {
            bookRepository.deleteById(id);
            return "book id: " + id + " deleted";
        }
        return "book not deleted, cannot found book id: " + id;
    }

    public String updateBook(Book book) {
        Optional<Book> optionalBookById = findBookById(book.getId());

        if (optionalBookById.isPresent()) {
            bookRepository.save(book);
            return "book id: " + book.getId() + "updated";
        }
        return "book not updated, cannot found id: " + book.getId() + "in DB";
    }
}
