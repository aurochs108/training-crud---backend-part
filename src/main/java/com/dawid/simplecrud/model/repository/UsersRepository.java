package com.dawid.simplecrud.model.repository;

import com.dawid.simplecrud.model.entity.users.Users;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends CrudRepository<Users, Integer> {

    public Users findByUsername(String username);
}
