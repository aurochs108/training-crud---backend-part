package com.dawid.simplecrud.model.repository;

import com.dawid.simplecrud.model.entity.book.Book;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends CrudRepository<Book, Integer> {

    public List<Book> findAllByOrderById();

}
