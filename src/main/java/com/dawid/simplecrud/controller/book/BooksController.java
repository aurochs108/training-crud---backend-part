package com.dawid.simplecrud.controller.book;

import com.dawid.simplecrud.model.entity.book.Book;
import com.dawid.simplecrud.model.service.book.BookService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@Secured({"ROLE_USER", "ROLE_ADMIN"})
@CrossOrigin(origins = "*")
@RequestMapping("/books")
public class BooksController {

    private BookService bookService;

    public BooksController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping(value = "/getAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Iterable<Book>> getAllBooks() {
        return ResponseEntity.ok().body(bookService.getAllBooks());
    }

    @PostMapping(path = "/saveBook", consumes = "application/json", produces = "application/json")
    public ResponseEntity<String> saveBook(@RequestBody Book book) {
        String savingStatus = bookService.saveBook(book);
        if (savingStatus.equals("book added")) {
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(savingStatus);
        }
        return ResponseEntity
                .status(HttpStatus.CONFLICT)
                .body(savingStatus);
    }

    @DeleteMapping("/deleteBook/{id}")
    public ResponseEntity<Object> deleteBook(@PathVariable int id, HttpServletRequest request,
                                             HttpServletResponse response) {
        String deleteStatus = bookService.deleteBookById(id);

        if (deleteStatus.startsWith("book not deleted")) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/updateBook")
    public ResponseEntity<Book> updateBook(@RequestBody Book book) {
        String updateStatus = bookService.updateBook(book);
        if (updateStatus.startsWith("book not updated")) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(book);
    }
}
