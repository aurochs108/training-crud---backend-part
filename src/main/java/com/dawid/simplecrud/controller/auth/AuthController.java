package com.dawid.simplecrud.controller.auth;

import com.dawid.simplecrud.model.dto.auth.AuthenticationDTO;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/auth")
public class AuthController {

    @GetMapping(path = "/role", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<? extends GrantedAuthority>> getAuthUserRole(Authentication authentication) {
        return ResponseEntity.ok().body(authentication.getAuthorities());
    }

    @GetMapping(path = "/basicAuth")
    public ResponseEntity<AuthenticationDTO> basicAuth() {
        return ResponseEntity.ok().body(new AuthenticationDTO("You are authenticated"));
    }
}
